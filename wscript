import os
import time

project = 'problems'
top = '.'
out = 'build'

f_release = 0

def release(ctx):
    global f_release
    f_release = 1


def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def configure(ctx):
    ctx.env.VERSION = os.popen('git rev-parse --short HEAD').read().rstrip()
    ctx.env.TIMESTAMP = str(int(time.time()))
    ctx.env.DEFINES = ['VERSION=0x'+ctx.env.VERSION, 'COMPILE_TIMESTAMP='+ctx.env.TIMESTAMP, 'DEBUG=0']
    print('→ configuring the project')
    ctx.find_program('strip', var='STRIP')
    ctx.find_program('gcc', var='CC')
    ctx.find_program('g++', var='CXX')
    ctx.env.LDFLAGS = '-fopenmp'.split()
    if f_release:
        ctx.env.CFLAGS = '-O4 -fwhole'.split()
    else:
        ctx.env.CFLAGS = '-O4 -g -ggdb -fPIC'.split()
    ctx.env.CFLAGS += '-Wno-unused-variable -Wno-format -Wno-unused-but-set-variable -Wall -fopenmp'.split()
    ctx.env.CXXFLAGS = [] + ctx.env.CFLAGS
    ctx.env.CFLAGS += '-std=gnu99'.split()
    ctx.env.CXXFLAGS += '-std=gnu++2a'.split()
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def clean(ctx):
    print('Cleaned.')

def build(ctx):
    source_excl = ['']
    source_objlib = ctx.path.ant_glob('src/lib/*.cc', excl = source_excl)
    source_chordnumbers = ctx.path.ant_glob('chordnumbers.cc', excl = source_excl)
    source_sixparts = ctx.path.ant_glob('sixparts.cc', excl = source_excl)
    source_earth = ctx.path.ant_glob('earth.cc', excl = source_excl)
    includes = ['.', 'src', 'src/lib']
    libs = ['pthread', 'mpfr']
    print(source_objlib)
    ctx.objects(target='objlib', includes = includes, source = source_objlib)
    ctx.program(target='chordnumbers', source = source_chordnumbers, use=['objlib'], lib=libs, includes = includes)
    ctx.program(target='sixparts', source = source_sixparts, use=['objlib'], lib=libs, includes = includes)
    ctx.program(target='earth', source = source_earth, use=['objlib'], lib=libs, includes = includes)
