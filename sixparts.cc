//g++ -std=gnu++20 -fopenmp -o sixparts sixparts.cc
#include <stdint.h>
#include <string.h>
#include <iostream>
#include <list>
// #include <format>

#include <omp.h>

#define num 2022

int main(int argc, char** argv)
{

#pragma omp parallel for
    for(int64_t a = 1; a <= num; a++){
        for(int64_t b = 1; b <= num; b++){
            int64_t ab = a * b;
            for(int64_t c = 1; c <= num; c++){
                int64_t abc = ab * c;
                for(int64_t d = 1; d <= num; d++){
                    for(int64_t e = 1; e <= num; e++){
                        int64_t de = d * e;
#pragma omp unroll full
#pragma omp simd
                        for(int64_t f = 1; f <= num; f++){
                            int64_t def = de * f;
                            //
                            int64_t abcde_ = abc * de;
                            int64_t ab_def = ab * def;
                            //
                            int64_t abc__f = abc * f;
                            int64_t abcd_f = abc__f * d;
                            int64_t abc_ef = abc__f * e;
                            //
                            int64_t __cdef = def * c;
                            int64_t a_cdef = __cdef * a;
                            int64_t _bcdef = __cdef * b;
                            //
                            int64_t abcdef = abc * def;
                            // 13 multiplications in total instead of 29
                            // 9 multiplications only inside loop
                            //printf("%ld, %ld, %ld, %ld, %ld, %ld\n", ab,  abc,  de, def, e, f);
                            if( (a + b + c + d + e + f) * abcdef == 2022 * (abcde_ + abcd_f + abc_ef + ab_def + a_cdef + _bcdef) ) {
                                printf("%d, %d, %d, %d, %d, %d\n", a, b, c, d, e, f);
                                //std::cout << std::format("{:^ 8}|{:^ 8}|{:^ 8}|{:^ 8}|{:^ 8}|{:^ 8}\n", a, b, c, d, e, f);
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}
