#include <stdint.h>
#include <string.h>
#include <atomic>
#include <tuple>
#include <iostream>
#include <list>
#include <vector>
#include <omp.h>

uint64_t *pair_matrix;
uint64_t *addr_matrix;
uint32_t* addr_vector;
std::atomic<int> addr_vector_len;

inline static int bitcount(uint64_t v) {
    typedef uint64_t T;
    v = v - ((v >> 1) & (T)~(T)0/3);
    v = (v & (T)~(T)0/15*3) + ((v >> 2) & (T)~(T)0/15*3);
    v = (v + (v >> 4)) & (T)~(T)0/255*15;
    T c = (T)(v * ((T)~(T)0/255)) >> (sizeof(T) - 1) * 8;
    return c;
}

std::tuple<int, int> pair_check(uint64_t *matrix, int n1, int n2) {
    std::atomic<uint64_t> *amatrix = (std::atomic<uint64_t> *)matrix;
    int v1, v2;
    if(n1 > n2) {v1 = n2; v2 = n1;}
    else {v1 = n1; v2 = n2;}
    v1 &= 0xFFFF;
    v2 &= 0xFFFF;
    uint32_t a1 = (v1 << 16) | v2;
    int b1 = a1 & 0x3F;
    a1 = a1 >> 6;
    uint64_t m1 = 1ULL << b1;
    auto r = std::atomic_fetch_or(&amatrix[a1], m1);
    return {(r & m1) == 0, a1};
}

int addr_check(uint64_t *matrix, int addr) {
    std::atomic<uint64_t> *amatrix = (std::atomic<uint64_t> *)matrix;
    uint32_t a = addr;
    int b = a & 0x3F;
    a = a >> 6;
    //    printf("c %d\n", a);
    uint64_t m = 1ULL << b;
    auto r = std::atomic_fetch_or(&amatrix[a], m);
    return (r & m) == 0;
}

void clear_tables() {
    int l = addr_vector_len;
#pragma omp parallel for
    for(int i = 0; i < l; i ++) {
        auto a = addr_vector[i];
        pair_matrix[a] = 0;
        addr_matrix[a >> 6] = 0;
    }
    addr_vector_len = 0;
}

int pair_count(int max_num, int flag_out = 0) {
    clear_tables();
    int num = 0;
#pragma omp parallel for reduction (+:num) shared(addr_vector)
    for(int j = 1; j < max_num; j ++) {
        for(int i = 0; i < max_num; i ++) {
            int v1 = i;
            int v2 = (i + j) % max_num;
            auto r = pair_check(pair_matrix, v1, v2);
            if(std::get<0>(r)) {
                //if(((v1+1)*(v1+1) + (v1+1)*(v2+1)) % abs((v1+1)*(v1+1) - (v1+1)*(v2+1)) == 0) {
                if(((v1+1) + (v1+1)) % abs((v1+1) - (v2+1)) == 0) {
                    int a = std::get<1>(r);
                    if(addr_check(addr_matrix, a)){
                        auto addr_vector_last = std::atomic_fetch_add(&addr_vector_len, 1);
                        addr_vector[addr_vector_last] = a;
                    }
                    num += 1;
                    if(flag_out)printf("[%d, %d]\n", v1 + 1, v2 + 1);
                }
            }
        }
    }
    return num;
}

int main(int argc, char** argv)
{

    if(argc < 2) {
        printf("usage: %s <max num>\n", argv[0]);
        return -1;
    }

    int max_num = atoi(argv[1]);

    pair_matrix = new uint64_t[65536ULL * 65536 / 64];
    addr_matrix = new uint64_t[65536ULL * 65536 / 64 / 64];
    addr_vector_len = 0;
    addr_vector = new uint32_t[65536ULL * 65536 / 64];

#pragma omp parallel for
    for(int i = 0; i < (65536LL * 65536 / 64); i ++) pair_matrix[i] = 0;
#pragma omp parallel for
    for(int i = 0; i < (65536LL * 65536 / 64 / 64); i ++)
        addr_matrix[i] = 0;
#pragma omp parallel for
    for(int i = 0; i < 65536LL * 65536 / 64; i ++)
        addr_vector[i] = 0;

    for(int i = 0; i < max_num + 1; i ++) {
        int num = pair_count(i);
        printf("%d\t%d\n", i, num);
    }
    //    int num = pair_count(max_num);
    //    printf("num = %d\n", num);
    return 0;
}
