#include <stdint.h>
#include <string.h>
#include <iostream>
#include <list>

#define MAXW 16
#define MAXH 4

typedef struct {
    int len;
    char sym;
    int list[MAXW * MAXH];
} cmp_lmnt;

int table[MAXW * MAXH];
cmp_lmnt table_check[10];

void table_init() {
    memset(table, 0, sizeof(table));
    for(int i = 0; i < 10; i ++){
        table_check[i].len = 0;
        table_check[i].sym = -1;
        //        for(int j = 0; j < MAXW * MAXH; j ++) table_check[i].list[j] = -1;
    }
}

int table_xy_to_index(int x, int y) {
    if(x > MAXW) return -1;
    if(x < 0) return -1;
    if(y > MAXH) return -1;
    if(y < 0) return -1;
    return x + y * MAXW;
}

int table_write_num(int64_t num, int x, int y) {
    int cell = table_xy_to_index(x, y);
    if(cell < 0) return cell;
    while(num) {
        int rem = num % 10;
        num = num / 10;
        table[cell] = rem;
        cell ++;
        x ++;
        if(x > MAXW) return -2;
    }
    return 0;
}

void table_print() {
    int cell = 0;
    for(int j = 0; j < MAXH; j ++) {
        for(int i = 0; i < MAXW; i ++) {
            std::cout << table[cell + MAXW - 1 - i] << " ";
        }
        cell += MAXW;
        std::cout << std::endl;
    }
    std::cout << "-------------" << std::endl;
}

void table_cmp_print() {
    for(int j = 0; j < 10; j ++) {
        cmp_lmnt &cmp = table_check[j];
        if(cmp.sym == -1) break;
        std::cout << "sym = " << cmp.sym << std::endl;
        std::cout << "len = " << cmp.len << std::endl;
        for(int i = 0; i < cmp.len; i ++) {
            std::cout << cmp.list[i] << " ";
        }
        std::cout << std::endl;
    }
}

int64_t intpow(int base, int power){
    int64_t r = 1;
    for(int i = 0; i < power; i++) r *= base;
    return r;
}

int table_find_sym(char sym){
    int i;
    int last = 0;
    for(i = 0; i < 10; i ++){
        if(table_check[i].sym == -1){
            table_check[i].sym = sym;
            return i;
        }
        if(table_check[i].sym == sym) return i;
    }
    return -1;
}

void table_set_cmp(const std::string &s, int y){
    int cell = table_xy_to_index(0, y);
    for(int i = s.length() - 1; i >= 0 ; i --) {
        char sym = s.c_str()[i];
        std::cout << sym;
        int isym = table_find_sym(sym);
        if(isym == -1) {
            std::cerr << "too many different symbols" << std::endl;
            exit(-1);
        }
        cmp_lmnt &cmp = table_check[isym];
        cmp.list[cmp.len] = cell;
        cmp.len += 1;
        cell ++;
    }
    std::cout << std::endl;
}

int table_compare(){
    for(int j = 0; j < 10; j ++) {
        cmp_lmnt &cmp = table_check[j];
        if(cmp.sym == -1) break;
        int cipher = table[cmp.list[0]];
        for(int i = 1; i < cmp.len; i ++) {
            if(cipher != table[cmp.list[i]]) return 0;
        }
    }
    return 1;
}

int table_chk_diff(){
    for(int j = 0; j < 10; j ++) {
        cmp_lmnt &cmp1 = table_check[j];
        if(cmp1.sym == -1) break;
        int cipher1 = table[cmp1.list[0]];
        for(int i = j + 1; i < 10; i ++) {
            cmp_lmnt &cmp2 = table_check[i];
            if(cmp2.sym == -1) break;
            int cipher2 = table[cmp2.list[0]];
            if(cipher1 == cipher2) return 0;
        }
    }
    return 1;
}

int main(int argc, char** argv)
{
    table_init();
    std::string s1, s2, s3;
    int l1, l2, l3;
    std::cin >> s1;
    std::cin >> s2;
    table_set_cmp(s1, 0);
    table_set_cmp(s1, 1);
    table_set_cmp(s2, 2);
    table_cmp_print();
    l1 = s1.length();
    //    std::cin >> s3;
    int l1l = intpow(10, l1 - 1);
    int l1r = intpow(10, l1);
    for(int i = l1l; i < l1r; i ++){
        table_write_num(i, 0, 0);
        table_write_num(i, 0, 1);
        table_write_num(i + i, 0, 2);
        if(table_compare() && table_chk_diff()){
            std::cout << " " << i << std::endl << " " << i << std::endl << i + i << std::endl;
            std::cout << "-------------" << std::endl;
            table_print();
        }
    }
    return 0;
}
