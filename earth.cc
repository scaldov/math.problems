//g++ -std=gnu++20 -fopenmp -o earth earth.cc -lmpfr
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <iostream>
#include <list>
#include <math.h>
#include <mpreal.h>

#include <omp.h>

auto fn = [](auto s, auto r, auto d){
//     std::cout << sin((s - d) / r) << std::endl;
    auto arg = (s - d) / r;
    auto sine = sin(arg);
    auto s2 = s * s;
    return sine * sine * (r * r + s2) - s2;
};

auto fn_lin = [](auto s, auto r, auto d){
    auto arg = (s - d) / r;
    auto sine = arg;
    auto s2 = s * s;
    return sine * sine * (r * r + s2) - s2;
};

auto fn_cubic = [](auto s, auto r, auto d){
    auto arg = (s - d) / r;
    auto sine = arg - arg * arg * arg / 6.0;
    auto s2 = s * s;
    return sine * sine * (r * r + s2) - s2;
};

template <typename T, typename F>
auto solve(T x0, T x1, F fn) {
    decltype(x0) x, y0, y1;
    y0 = fn(x0);
    y1 = fn(x1);
    while(true) {
        x = (x0 + x1) * 0.5;
        mpfr::mpreal y = fn(x);
//        std::cout << x0 << ", " << y0 << " | " << x << ", " << y << " | " << x1 << ", " << y1 << std::endl;
        std::cout << x << ", " << y << std::endl << "------------------" << std::endl;
        if(fabs(x1 - x0) < 1e-100 || x1 == x0) break;
//         if(x1 == x0) break;
        if(y * y0 < 0) {
            y1 = y;
            x1 = x;
        } else {
            y0 = y;
            x0 = x;
        }
//         usleep(100000);
    }
    return x;
}

int main(int argc, char** argv)
{
    std::cout.precision(256);
    mpfr::mpreal::set_default_prec(1024);
    mpfr::mpreal r_earth = 6400000.0;
    mpfr::mpreal halfdelta = 0.005;
    mpfr::mpreal s0 = 6000.;
    mpfr::mpreal s1 = 10000.;
    auto fn = [r_earth, halfdelta](mpfr::mpreal s){return ::fn(s, r_earth, halfdelta);};
    mpfr::mpreal s = solve(s0, s1, fn);
    mpfr::mpreal t = sqrt(s * s + r_earth * r_earth);
    mpfr::mpreal h = t - r_earth;
    std::cout << s << std::endl;
    std::cout << t << std::endl;
    std::cout << h << std::endl;
    return 0;
}
