//g++ -std=gnu++20 -fopenmp -o earth earth.cc -lmpfr
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <omp.h>

void fn(int *p, int a) {
    *p += a;
}

int main(int argc, char** argv)
{
    const int x = 8;
//    *(&x) += 10;
//    int *p = &x;
//    *p += 10;
    fn(&x, 10);
    printf("%d \n", x);
    return 0;
}
